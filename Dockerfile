#
# Simple conatiner image to provide python and a few utils used for demos.
# Pulling a UBI from registry.access.redhat.com does not require auth.
#
FROM registry.access.redhat.com/ubi8/ubi
LABEL maintainer="bkozdemba at gmail dot com"

#
# Install packages.
#
RUN dnf -y update; dnf -y clean all
# RUN dnf -y install python3 iputils e2fsprogs --setopt install_weak_deps=false; dnf -y clean all
RUN dnf -y install python3 iputils --setopt install_weak_deps=false; dnf -y clean all

#
# Create a /healthz endpoint
#
RUN mkdir healthz
RUN echo "OK" >> healthz/index.html
RUN cp /proc/net/fib_trie healthz/net.html

#
# Add a default Web page and expose port 8080.
#
RUN echo "The Python http server is listening on port 8080" > index.html
EXPOSE 8080
#
# Start the python3 httpd service.
#
USER 1001
CMD [ "/usr/bin/python3",  "-m", "http.server", "8080" ]
